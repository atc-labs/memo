# PowerShell
You can use PowerShell, which offers more flexibility and memory management than findstr. Open PowerShell and run the following command to search for the string recursively in all files:

```
# powershell
Get-ChildItem -Recurse | Select-String -Pattern "localhost:7823"
```
This command will search for the specified string in all files within the current directory and its subdirectories.

# Third-Party Tools
There are several third-party tools designed for searching files and text content efficiently. One popular option is grep for Windows, which is part of the GNU utilities. You can install it via a package manager like Chocolatey:

```
# powershell
choco install grep
```
Once installed, you can use grep to search for the string:

```
# powershell
grep -r "localhost:7823" C:\
```

# Windows Subsystem for Linux (WSL)
If you're comfortable with Linux-like tools, you can use the Windows Subsystem for Linux (WSL) to run powerful commands like grep or find within your Windows files. This can help overcome memory limitations. Here's an example:

Install WSL if you haven't already (follow the official documentation).
- Open WSL terminal.
- Use grep or find commands as you would on a regular Linux system.
